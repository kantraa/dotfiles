#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '


##-----------------------------------------------------
## alias
if [ -f /home/neonvortex/.config/synth-shell/alias.sh ] && [ -n "$( echo $- | grep i )" ]; then
	source /home/neonvortex/.config/synth-shell/alias.sh
fi

##-----------------------------------------------------
## better-history
if [ -f /home/neonvortex/.config/synth-shell/better-history.sh ] && [ -n "$( echo $- | grep i )" ]; then
	source /home/neonvortex/.config/synth-shell/better-history.sh
fi

##-----------------------------------------------------
## powerline prompt
source ~/.bash-powerline-ng.sh
##-----------------------------------------------------
## aliases
alias server="ssh knerdfox@213.160.72.126"
alias brc="source .bashrc && clear"
alias b="vim ~/.config/bspwm/bspwmrc"
alias s="vim ~/.config/sxhkd/sxhkdrc"
alias p="vim ~/.config/polybar/config.ini"
alias l="~/.config/polybar/launch.sh"
##-----------------------------------------------------
## bash insulter
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi
##-----------------------------------------------------
## shell color scripts
colorscript random
eval "$(thefuck --alias)"
##-----------------------------------------------------
## path
export PATH="~/.local/bin:$PATH"
